import{bD as ie,h as t,am as L,c as v,an as R,d as V,f as B,ak as Z,a7 as M,a9 as Se,ab as Pe,aa as Te,a8 as ze,u as be,b as Q,bE as Ae,ad as q,g as ye,a as b,bF as ve,b2 as we,ar as Le,l as X,as as D,at as De,bG as ge,r as K,bH as Me,bk as pe,bI as W,bJ as Fe,bi as Ve,aD as te,aS as He,aT as ne,aU as le,bK as We,az as qe,bL as Xe,ah as Ie,aQ as Ge,bh as Ke,N as Ye,z as oe,bM as Ze,bN as Qe,bO as Je,br as et,af as xe,aR as tt,bP as rt,F as it,bQ as nt,b5 as Re,W as ot}from"./index-9dd63246.js";const at=ie("attach",t("svg",{viewBox:"0 0 16 16",version:"1.1",xmlns:"http://www.w3.org/2000/svg"},t("g",{stroke:"none","stroke-width":"1",fill:"none","fill-rule":"evenodd"},t("g",{fill:"currentColor","fill-rule":"nonzero"},t("path",{d:"M3.25735931,8.70710678 L7.85355339,4.1109127 C8.82986412,3.13460197 10.4127766,3.13460197 11.3890873,4.1109127 C12.365398,5.08722343 12.365398,6.67013588 11.3890873,7.64644661 L6.08578644,12.9497475 C5.69526215,13.3402718 5.06209717,13.3402718 4.67157288,12.9497475 C4.28104858,12.5592232 4.28104858,11.9260582 4.67157288,11.5355339 L9.97487373,6.23223305 C10.1701359,6.0369709 10.1701359,5.72038841 9.97487373,5.52512627 C9.77961159,5.32986412 9.4630291,5.32986412 9.26776695,5.52512627 L3.96446609,10.8284271 C3.18341751,11.6094757 3.18341751,12.8758057 3.96446609,13.6568542 C4.74551468,14.4379028 6.01184464,14.4379028 6.79289322,13.6568542 L12.0961941,8.35355339 C13.4630291,6.98671837 13.4630291,4.77064094 12.0961941,3.40380592 C10.7293591,2.0369709 8.51328163,2.0369709 7.14644661,3.40380592 L2.55025253,8 C2.35499039,8.19526215 2.35499039,8.51184464 2.55025253,8.70710678 C2.74551468,8.90236893 3.06209717,8.90236893 3.25735931,8.70710678 Z"}))))),lt=ie("trash",t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512"},t("path",{d:"M432,144,403.33,419.74A32,32,0,0,1,371.55,448H140.46a32,32,0,0,1-31.78-28.26L80,144",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}),t("rect",{x:"32",y:"64",width:"448",height:"80",rx:"16",ry:"16",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}),t("line",{x1:"312",y1:"240",x2:"200",y2:"352",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}),t("line",{x1:"312",y1:"352",x2:"200",y2:"240",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}))),st=ie("download",t("svg",{viewBox:"0 0 16 16",version:"1.1",xmlns:"http://www.w3.org/2000/svg"},t("g",{stroke:"none","stroke-width":"1",fill:"none","fill-rule":"evenodd"},t("g",{fill:"currentColor","fill-rule":"nonzero"},t("path",{d:"M3.5,13 L12.5,13 C12.7761424,13 13,13.2238576 13,13.5 C13,13.7454599 12.8231248,13.9496084 12.5898756,13.9919443 L12.5,14 L3.5,14 C3.22385763,14 3,13.7761424 3,13.5 C3,13.2545401 3.17687516,13.0503916 3.41012437,13.0080557 L3.5,13 L12.5,13 L3.5,13 Z M7.91012437,1.00805567 L8,1 C8.24545989,1 8.44960837,1.17687516 8.49194433,1.41012437 L8.5,1.5 L8.5,10.292 L11.1819805,7.6109127 C11.3555469,7.43734635 11.6249713,7.4180612 11.8198394,7.55305725 L11.8890873,7.6109127 C12.0626536,7.78447906 12.0819388,8.05390346 11.9469427,8.2487716 L11.8890873,8.31801948 L8.35355339,11.8535534 C8.17998704,12.0271197 7.91056264,12.0464049 7.7156945,11.9114088 L7.64644661,11.8535534 L4.1109127,8.31801948 C3.91565056,8.12275734 3.91565056,7.80617485 4.1109127,7.6109127 C4.28447906,7.43734635 4.55390346,7.4180612 4.7487716,7.55305725 L4.81801948,7.6109127 L7.5,10.292 L7.5,1.5 C7.5,1.25454011 7.67687516,1.05039163 7.91012437,1.00805567 L8,1 L7.91012437,1.00805567 Z"}))))),dt=ie("cancel",t("svg",{viewBox:"0 0 16 16",version:"1.1",xmlns:"http://www.w3.org/2000/svg"},t("g",{stroke:"none","stroke-width":"1",fill:"none","fill-rule":"evenodd"},t("g",{fill:"currentColor","fill-rule":"nonzero"},t("path",{d:"M2.58859116,2.7156945 L2.64644661,2.64644661 C2.82001296,2.47288026 3.08943736,2.45359511 3.2843055,2.58859116 L3.35355339,2.64644661 L8,7.293 L12.6464466,2.64644661 C12.8417088,2.45118446 13.1582912,2.45118446 13.3535534,2.64644661 C13.5488155,2.84170876 13.5488155,3.15829124 13.3535534,3.35355339 L8.707,8 L13.3535534,12.6464466 C13.5271197,12.820013 13.5464049,13.0894374 13.4114088,13.2843055 L13.3535534,13.3535534 C13.179987,13.5271197 12.9105626,13.5464049 12.7156945,13.4114088 L12.6464466,13.3535534 L8,8.707 L3.35355339,13.3535534 C3.15829124,13.5488155 2.84170876,13.5488155 2.64644661,13.3535534 C2.45118446,13.1582912 2.45118446,12.8417088 2.64644661,12.6464466 L7.293,8 L2.64644661,3.35355339 C2.47288026,3.17998704 2.45359511,2.91056264 2.58859116,2.7156945 L2.64644661,2.64644661 L2.58859116,2.7156945 Z"}))))),ct=ie("retry",t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 512 512"},t("path",{d:"M320,146s24.36-12-64-12A160,160,0,1,0,416,294",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-miterlimit: 10; stroke-width: 32px;"}),t("polyline",{points:"256 58 336 138 256 218",style:"fill: none; stroke: currentcolor; stroke-linecap: round; stroke-linejoin: round; stroke-width: 32px;"}))),ut=L([v("progress",{display:"inline-block"},[v("progress-icon",`
 color: var(--n-icon-color);
 transition: color .3s var(--n-bezier);
 `),R("line",`
 width: 100%;
 display: block;
 `,[v("progress-content",`
 display: flex;
 align-items: center;
 `,[v("progress-graph",{flex:1})]),v("progress-custom-content",{marginLeft:"14px"}),v("progress-icon",`
 width: 30px;
 padding-left: 14px;
 height: var(--n-icon-size-line);
 line-height: var(--n-icon-size-line);
 font-size: var(--n-icon-size-line);
 `,[R("as-text",`
 color: var(--n-text-color-line-outer);
 text-align: center;
 width: 40px;
 font-size: var(--n-font-size);
 padding-left: 4px;
 transition: color .3s var(--n-bezier);
 `)])]),R("circle, dashboard",{width:"120px"},[v("progress-custom-content",`
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 justify-content: center;
 `),v("progress-text",`
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 color: inherit;
 font-size: var(--n-font-size-circle);
 color: var(--n-text-color-circle);
 font-weight: var(--n-font-weight-circle);
 transition: color .3s var(--n-bezier);
 white-space: nowrap;
 `),v("progress-icon",`
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 color: var(--n-icon-color);
 font-size: var(--n-icon-size-circle);
 `)]),R("multiple-circle",`
 width: 200px;
 color: inherit;
 `,[v("progress-text",`
 font-weight: var(--n-font-weight-circle);
 color: var(--n-text-color-circle);
 position: absolute;
 left: 50%;
 top: 50%;
 transform: translateX(-50%) translateY(-50%);
 display: flex;
 align-items: center;
 justify-content: center;
 transition: color .3s var(--n-bezier);
 `)]),v("progress-content",{position:"relative"}),v("progress-graph",{position:"relative"},[v("progress-graph-circle",[L("svg",{verticalAlign:"bottom"}),v("progress-graph-circle-fill",`
 stroke: var(--n-fill-color);
 transition:
 opacity .3s var(--n-bezier),
 stroke .3s var(--n-bezier),
 stroke-dasharray .3s var(--n-bezier);
 `,[R("empty",{opacity:0})]),v("progress-graph-circle-rail",`
 transition: stroke .3s var(--n-bezier);
 overflow: hidden;
 stroke: var(--n-rail-color);
 `)]),v("progress-graph-line",[R("indicator-inside",[v("progress-graph-line-rail",`
 height: 16px;
 line-height: 16px;
 border-radius: 10px;
 `,[v("progress-graph-line-fill",`
 height: inherit;
 border-radius: 10px;
 `),v("progress-graph-line-indicator",`
 background: #0000;
 white-space: nowrap;
 text-align: right;
 margin-left: 14px;
 margin-right: 14px;
 height: inherit;
 font-size: 12px;
 color: var(--n-text-color-line-inner);
 transition: color .3s var(--n-bezier);
 `)])]),R("indicator-inside-label",`
 height: 16px;
 display: flex;
 align-items: center;
 `,[v("progress-graph-line-rail",`
 flex: 1;
 transition: background-color .3s var(--n-bezier);
 `),v("progress-graph-line-indicator",`
 background: var(--n-fill-color);
 font-size: 12px;
 transform: translateZ(0);
 display: flex;
 vertical-align: middle;
 height: 16px;
 line-height: 16px;
 padding: 0 10px;
 border-radius: 10px;
 position: absolute;
 white-space: nowrap;
 color: var(--n-text-color-line-inner);
 transition:
 right .2s var(--n-bezier),
 color .3s var(--n-bezier),
 background-color .3s var(--n-bezier);
 `)]),v("progress-graph-line-rail",`
 position: relative;
 overflow: hidden;
 height: var(--n-rail-height);
 border-radius: 5px;
 background-color: var(--n-rail-color);
 transition: background-color .3s var(--n-bezier);
 `,[v("progress-graph-line-fill",`
 background: var(--n-fill-color);
 position: relative;
 border-radius: 5px;
 height: inherit;
 width: 100%;
 max-width: 0%;
 transition:
 background-color .3s var(--n-bezier),
 max-width .2s var(--n-bezier);
 `,[R("processing",[L("&::after",`
 content: "";
 background-image: var(--n-line-bg-processing);
 animation: progress-processing-animation 2s var(--n-bezier) infinite;
 `)])])])])])]),L("@keyframes progress-processing-animation",`
 0% {
 position: absolute;
 left: 0;
 top: 0;
 bottom: 0;
 right: 100%;
 opacity: 1;
 }
 66% {
 position: absolute;
 left: 0;
 top: 0;
 bottom: 0;
 right: 0;
 opacity: 0;
 }
 100% {
 position: absolute;
 left: 0;
 top: 0;
 bottom: 0;
 right: 0;
 opacity: 0;
 }
 `)]),ft={success:t(Se,null),error:t(Pe,null),warning:t(Te,null),info:t(ze,null)},ht=V({name:"ProgressLine",props:{clsPrefix:{type:String,required:!0},percentage:{type:Number,default:0},railColor:String,railStyle:[String,Object],fillColor:String,status:{type:String,required:!0},indicatorPlacement:{type:String,required:!0},indicatorTextColor:String,unit:{type:String,default:"%"},processing:{type:Boolean,required:!0},showIndicator:{type:Boolean,required:!0},height:[String,Number],railBorderRadius:[String,Number],fillBorderRadius:[String,Number]},setup(e,{slots:i}){const n=B(()=>Z(e.height)),r=B(()=>e.railBorderRadius!==void 0?Z(e.railBorderRadius):e.height!==void 0?Z(e.height,{c:.5}):""),o=B(()=>e.fillBorderRadius!==void 0?Z(e.fillBorderRadius):e.railBorderRadius!==void 0?Z(e.railBorderRadius):e.height!==void 0?Z(e.height,{c:.5}):"");return()=>{const{indicatorPlacement:a,railColor:c,railStyle:u,percentage:d,unit:s,indicatorTextColor:l,status:f,showIndicator:m,fillColor:g,processing:C,clsPrefix:x}=e;return t("div",{class:`${x}-progress-content`,role:"none"},t("div",{class:`${x}-progress-graph`,"aria-hidden":!0},t("div",{class:[`${x}-progress-graph-line`,{[`${x}-progress-graph-line--indicator-${a}`]:!0}]},t("div",{class:`${x}-progress-graph-line-rail`,style:[{backgroundColor:c,height:n.value,borderRadius:r.value},u]},t("div",{class:[`${x}-progress-graph-line-fill`,C&&`${x}-progress-graph-line-fill--processing`],style:{maxWidth:`${e.percentage}%`,backgroundColor:g,height:n.value,lineHeight:n.value,borderRadius:o.value}},a==="inside"?t("div",{class:`${x}-progress-graph-line-indicator`,style:{color:l}},i.default?i.default():`${d}${s}`):null)))),m&&a==="outside"?t("div",null,i.default?t("div",{class:`${x}-progress-custom-content`,style:{color:l},role:"none"},i.default()):f==="default"?t("div",{role:"none",class:`${x}-progress-icon ${x}-progress-icon--as-text`,style:{color:l}},d,s):t("div",{class:`${x}-progress-icon`,"aria-hidden":!0},t(M,{clsPrefix:x},{default:()=>ft[f]}))):null)}}}),gt={success:t(Se,null),error:t(Pe,null),warning:t(Te,null),info:t(ze,null)},pt=V({name:"ProgressCircle",props:{clsPrefix:{type:String,required:!0},status:{type:String,required:!0},strokeWidth:{type:Number,required:!0},fillColor:String,railColor:String,railStyle:[String,Object],percentage:{type:Number,default:0},offsetDegree:{type:Number,default:0},showIndicator:{type:Boolean,required:!0},indicatorTextColor:String,unit:String,viewBoxWidth:{type:Number,required:!0},gapDegree:{type:Number,required:!0},gapOffsetDegree:{type:Number,default:0}},setup(e,{slots:i}){function n(r,o,a){const{gapDegree:c,viewBoxWidth:u,strokeWidth:d}=e,s=50,l=0,f=s,m=0,g=2*s,C=50+d/2,x=`M ${C},${C} m ${l},${f}
      a ${s},${s} 0 1 1 ${m},${-g}
      a ${s},${s} 0 1 1 ${-m},${g}`,_=Math.PI*2*s,z={stroke:a,strokeDasharray:`${r/100*(_-c)}px ${u*8}px`,strokeDashoffset:`-${c/2}px`,transformOrigin:o?"center":void 0,transform:o?`rotate(${o}deg)`:void 0};return{pathString:x,pathStyle:z}}return()=>{const{fillColor:r,railColor:o,strokeWidth:a,offsetDegree:c,status:u,percentage:d,showIndicator:s,indicatorTextColor:l,unit:f,gapOffsetDegree:m,clsPrefix:g}=e,{pathString:C,pathStyle:x}=n(100,0,o),{pathString:_,pathStyle:z}=n(d,c,r),P=100+a;return t("div",{class:`${g}-progress-content`,role:"none"},t("div",{class:`${g}-progress-graph`,"aria-hidden":!0},t("div",{class:`${g}-progress-graph-circle`,style:{transform:m?`rotate(${m}deg)`:void 0}},t("svg",{viewBox:`0 0 ${P} ${P}`},t("g",null,t("path",{class:`${g}-progress-graph-circle-rail`,d:C,"stroke-width":a,"stroke-linecap":"round",fill:"none",style:x})),t("g",null,t("path",{class:[`${g}-progress-graph-circle-fill`,d===0&&`${g}-progress-graph-circle-fill--empty`],d:_,"stroke-width":a,"stroke-linecap":"round",fill:"none",style:z}))))),s?t("div",null,i.default?t("div",{class:`${g}-progress-custom-content`,role:"none"},i.default()):u!=="default"?t("div",{class:`${g}-progress-icon`,"aria-hidden":!0},t(M,{clsPrefix:g},{default:()=>gt[u]})):t("div",{class:`${g}-progress-text`,style:{color:l},role:"none"},t("span",{class:`${g}-progress-text__percentage`},d),t("span",{class:`${g}-progress-text__unit`},f))):null)}}});function Ce(e,i,n=100){return`m ${n/2} ${n/2-e} a ${e} ${e} 0 1 1 0 ${2*e} a ${e} ${e} 0 1 1 0 -${2*e}`}const vt=V({name:"ProgressMultipleCircle",props:{clsPrefix:{type:String,required:!0},viewBoxWidth:{type:Number,required:!0},percentage:{type:Array,default:[0]},strokeWidth:{type:Number,required:!0},circleGap:{type:Number,required:!0},showIndicator:{type:Boolean,required:!0},fillColor:{type:Array,default:()=>[]},railColor:{type:Array,default:()=>[]},railStyle:{type:Array,default:()=>[]}},setup(e,{slots:i}){const n=B(()=>e.percentage.map((o,a)=>`${Math.PI*o/100*(e.viewBoxWidth/2-e.strokeWidth/2*(1+2*a)-e.circleGap*a)*2}, ${e.viewBoxWidth*8}`));return()=>{const{viewBoxWidth:r,strokeWidth:o,circleGap:a,showIndicator:c,fillColor:u,railColor:d,railStyle:s,percentage:l,clsPrefix:f}=e;return t("div",{class:`${f}-progress-content`,role:"none"},t("div",{class:`${f}-progress-graph`,"aria-hidden":!0},t("div",{class:`${f}-progress-graph-circle`},t("svg",{viewBox:`0 0 ${r} ${r}`},l.map((m,g)=>t("g",{key:g},t("path",{class:`${f}-progress-graph-circle-rail`,d:Ce(r/2-o/2*(1+2*g)-a*g,o,r),"stroke-width":o,"stroke-linecap":"round",fill:"none",style:[{strokeDashoffset:0,stroke:d[g]},s[g]]}),t("path",{class:[`${f}-progress-graph-circle-fill`,m===0&&`${f}-progress-graph-circle-fill--empty`],d:Ce(r/2-o/2*(1+2*g)-a*g,o,r),"stroke-width":o,"stroke-linecap":"round",fill:"none",style:{strokeDasharray:n.value[g],strokeDashoffset:0,stroke:u[g]}})))))),c&&i.default?t("div",null,t("div",{class:`${f}-progress-text`},i.default())):null)}}}),mt=Object.assign(Object.assign({},Q.props),{processing:Boolean,type:{type:String,default:"line"},gapDegree:Number,gapOffsetDegree:Number,status:{type:String,default:"default"},railColor:[String,Array],railStyle:[String,Array],color:[String,Array],viewBoxWidth:{type:Number,default:100},strokeWidth:{type:Number,default:7},percentage:[Number,Array],unit:{type:String,default:"%"},showIndicator:{type:Boolean,default:!0},indicatorPosition:{type:String,default:"outside"},indicatorPlacement:{type:String,default:"outside"},indicatorTextColor:String,circleGap:{type:Number,default:1},height:Number,borderRadius:[String,Number],fillBorderRadius:[String,Number],offsetDegree:Number}),bt=V({name:"Progress",props:mt,setup(e){const i=B(()=>e.indicatorPlacement||e.indicatorPosition),n=B(()=>{if(e.gapDegree||e.gapDegree===0)return e.gapDegree;if(e.type==="dashboard")return 75}),{mergedClsPrefixRef:r,inlineThemeDisabled:o}=be(e),a=Q("Progress","-progress",ut,Ae,e,r),c=B(()=>{const{status:d}=e,{common:{cubicBezierEaseInOut:s},self:{fontSize:l,fontSizeCircle:f,railColor:m,railHeight:g,iconSizeCircle:C,iconSizeLine:x,textColorCircle:_,textColorLineInner:z,textColorLineOuter:P,lineBgProcessing:j,fontWeightCircle:$,[q("iconColor",d)]:p,[q("fillColor",d)]:w}}=a.value;return{"--n-bezier":s,"--n-fill-color":w,"--n-font-size":l,"--n-font-size-circle":f,"--n-font-weight-circle":$,"--n-icon-color":p,"--n-icon-size-circle":C,"--n-icon-size-line":x,"--n-line-bg-processing":j,"--n-rail-color":m,"--n-rail-height":g,"--n-text-color-circle":_,"--n-text-color-line-inner":z,"--n-text-color-line-outer":P}}),u=o?ye("progress",B(()=>e.status[0]),c,e):void 0;return{mergedClsPrefix:r,mergedIndicatorPlacement:i,gapDeg:n,cssVars:o?void 0:c,themeClass:u==null?void 0:u.themeClass,onRender:u==null?void 0:u.onRender}},render(){const{type:e,cssVars:i,indicatorTextColor:n,showIndicator:r,status:o,railColor:a,railStyle:c,color:u,percentage:d,viewBoxWidth:s,strokeWidth:l,mergedIndicatorPlacement:f,unit:m,borderRadius:g,fillBorderRadius:C,height:x,processing:_,circleGap:z,mergedClsPrefix:P,gapDeg:j,gapOffsetDegree:$,themeClass:p,$slots:w,onRender:y}=this;return y==null||y(),t("div",{class:[p,`${P}-progress`,`${P}-progress--${e}`,`${P}-progress--${o}`],style:i,"aria-valuemax":100,"aria-valuemin":0,"aria-valuenow":d,role:e==="circle"||e==="line"||e==="dashboard"?"progressbar":"none"},e==="circle"||e==="dashboard"?t(pt,{clsPrefix:P,status:o,showIndicator:r,indicatorTextColor:n,railColor:a,fillColor:u,railStyle:c,offsetDegree:this.offsetDegree,percentage:d,viewBoxWidth:s,strokeWidth:l,gapDegree:j===void 0?e==="dashboard"?75:0:j,gapOffsetDegree:$,unit:m},w):e==="line"?t(ht,{clsPrefix:P,status:o,showIndicator:r,indicatorTextColor:n,railColor:a,fillColor:u,railStyle:c,percentage:d,processing:_,indicatorPlacement:f,unit:m,fillBorderRadius:C,railBorderRadius:g,height:x},w):e==="multiple-circle"?t(vt,{clsPrefix:P,strokeWidth:l,railColor:a,fillColor:u,railStyle:c,viewBoxWidth:s,percentage:d,showIndicator:r,circleGap:z},w):null)}}),yt=v("switch",`
 height: var(--n-height);
 min-width: var(--n-width);
 vertical-align: middle;
 user-select: none;
 -webkit-user-select: none;
 display: inline-flex;
 outline: none;
 justify-content: center;
 align-items: center;
`,[b("children-placeholder",`
 height: var(--n-rail-height);
 display: flex;
 flex-direction: column;
 overflow: hidden;
 pointer-events: none;
 visibility: hidden;
 `),b("rail-placeholder",`
 display: flex;
 flex-wrap: none;
 `),b("button-placeholder",`
 width: calc(1.75 * var(--n-rail-height));
 height: var(--n-rail-height);
 `),v("base-loading",`
 position: absolute;
 top: 50%;
 left: 50%;
 transform: translateX(-50%) translateY(-50%);
 font-size: calc(var(--n-button-width) - 4px);
 color: var(--n-loading-color);
 transition: color .3s var(--n-bezier);
 `,[ve({left:"50%",top:"50%",originalTransform:"translateX(-50%) translateY(-50%)"})]),b("checked, unchecked",`
 transition: color .3s var(--n-bezier);
 color: var(--n-text-color);
 box-sizing: border-box;
 position: absolute;
 white-space: nowrap;
 top: 0;
 bottom: 0;
 display: flex;
 align-items: center;
 line-height: 1;
 `),b("checked",`
 right: 0;
 padding-right: calc(1.25 * var(--n-rail-height) - var(--n-offset));
 `),b("unchecked",`
 left: 0;
 justify-content: flex-end;
 padding-left: calc(1.25 * var(--n-rail-height) - var(--n-offset));
 `),L("&:focus",[b("rail",`
 box-shadow: var(--n-box-shadow-focus);
 `)]),R("round",[b("rail","border-radius: calc(var(--n-rail-height) / 2);",[b("button","border-radius: calc(var(--n-button-height) / 2);")])]),we("disabled",[we("icon",[R("rubber-band",[R("pressed",[b("rail",[b("button","max-width: var(--n-button-width-pressed);")])]),b("rail",[L("&:active",[b("button","max-width: var(--n-button-width-pressed);")])]),R("active",[R("pressed",[b("rail",[b("button","left: calc(100% - var(--n-offset) - var(--n-button-width-pressed));")])]),b("rail",[L("&:active",[b("button","left: calc(100% - var(--n-offset) - var(--n-button-width-pressed));")])])])])])]),R("active",[b("rail",[b("button","left: calc(100% - var(--n-button-width) - var(--n-offset))")])]),b("rail",`
 overflow: hidden;
 height: var(--n-rail-height);
 min-width: var(--n-rail-width);
 border-radius: var(--n-rail-border-radius);
 cursor: pointer;
 position: relative;
 transition:
 opacity .3s var(--n-bezier),
 background .3s var(--n-bezier),
 box-shadow .3s var(--n-bezier);
 background-color: var(--n-rail-color);
 `,[b("button-icon",`
 color: var(--n-icon-color);
 transition: color .3s var(--n-bezier);
 font-size: calc(var(--n-button-height) - 4px);
 position: absolute;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 display: flex;
 justify-content: center;
 align-items: center;
 line-height: 1;
 `,[ve()]),b("button",`
 align-items: center; 
 top: var(--n-offset);
 left: var(--n-offset);
 height: var(--n-button-height);
 width: var(--n-button-width-pressed);
 max-width: var(--n-button-width);
 border-radius: var(--n-button-border-radius);
 background-color: var(--n-button-color);
 box-shadow: var(--n-button-box-shadow);
 box-sizing: border-box;
 cursor: inherit;
 content: "";
 position: absolute;
 transition:
 background-color .3s var(--n-bezier),
 left .3s var(--n-bezier),
 opacity .3s var(--n-bezier),
 max-width .3s var(--n-bezier),
 box-shadow .3s var(--n-bezier);
 `)]),R("active",[b("rail","background-color: var(--n-rail-color-active);")]),R("loading",[b("rail",`
 cursor: wait;
 `)]),R("disabled",[b("rail",`
 cursor: not-allowed;
 opacity: .5;
 `)])]),wt=Object.assign(Object.assign({},Q.props),{size:{type:String,default:"medium"},value:{type:[String,Number,Boolean],default:void 0},loading:Boolean,defaultValue:{type:[String,Number,Boolean],default:!1},disabled:{type:Boolean,default:void 0},round:{type:Boolean,default:!0},"onUpdate:value":[Function,Array],onUpdateValue:[Function,Array],checkedValue:{type:[String,Number,Boolean],default:!0},uncheckedValue:{type:[String,Number,Boolean],default:!1},railStyle:Function,rubberBand:{type:Boolean,default:!0},onChange:[Function,Array]});let ee;const Vt=V({name:"Switch",props:wt,setup(e){ee===void 0&&(typeof CSS<"u"?typeof CSS.supports<"u"?ee=CSS.supports("width","max(1px)"):ee=!1:ee=!0);const{mergedClsPrefixRef:i,inlineThemeDisabled:n}=be(e),r=Q("Switch","-switch",yt,Me,e,i),o=Le(e),{mergedSizeRef:a,mergedDisabledRef:c}=o,u=X(e.defaultValue),d=D(e,"value"),s=De(d,u),l=B(()=>s.value===e.checkedValue),f=X(!1),m=X(!1),g=B(()=>{const{railStyle:k}=e;if(k)return k({focused:m.value,checked:l.value})});function C(k){const{"onUpdate:value":h,onChange:S,onUpdateValue:T}=e,{nTriggerFormInput:E,nTriggerFormChange:U}=o;h&&te(h,k),T&&te(T,k),S&&te(S,k),u.value=k,E(),U()}function x(){const{nTriggerFormFocus:k}=o;k()}function _(){const{nTriggerFormBlur:k}=o;k()}function z(){e.loading||c.value||(s.value!==e.checkedValue?C(e.checkedValue):C(e.uncheckedValue))}function P(){m.value=!0,x()}function j(){m.value=!1,_(),f.value=!1}function $(k){e.loading||c.value||k.key===" "&&(s.value!==e.checkedValue?C(e.checkedValue):C(e.uncheckedValue),f.value=!1)}function p(k){e.loading||c.value||k.key===" "&&(k.preventDefault(),f.value=!0)}const w=B(()=>{const{value:k}=a,{self:{opacityDisabled:h,railColor:S,railColorActive:T,buttonBoxShadow:E,buttonColor:U,boxShadowFocus:N,loadingColor:F,textColor:I,iconColor:A,[q("buttonHeight",k)]:O,[q("buttonWidth",k)]:Y,[q("buttonWidthPressed",k)]:se,[q("railHeight",k)]:H,[q("railWidth",k)]:G,[q("railBorderRadius",k)]:de,[q("buttonBorderRadius",k)]:ce},common:{cubicBezierEaseInOut:Ee}}=r.value;let ue,fe,he;return ee?(ue=`calc((${H} - ${O}) / 2)`,fe=`max(${H}, ${O})`,he=`max(${G}, calc(${G} + ${O} - ${H}))`):(ue=pe((W(H)-W(O))/2),fe=pe(Math.max(W(H),W(O))),he=W(H)>W(O)?G:pe(W(G)+W(O)-W(H))),{"--n-bezier":Ee,"--n-button-border-radius":ce,"--n-button-box-shadow":E,"--n-button-color":U,"--n-button-width":Y,"--n-button-width-pressed":se,"--n-button-height":O,"--n-height":fe,"--n-offset":ue,"--n-opacity-disabled":h,"--n-rail-border-radius":de,"--n-rail-color":S,"--n-rail-color-active":T,"--n-rail-height":H,"--n-rail-width":G,"--n-width":he,"--n-box-shadow-focus":N,"--n-loading-color":F,"--n-text-color":I,"--n-icon-color":A}}),y=n?ye("switch",B(()=>a.value[0]),w,e):void 0;return{handleClick:z,handleBlur:j,handleFocus:P,handleKeyup:$,handleKeydown:p,mergedRailStyle:g,pressed:f,mergedClsPrefix:i,mergedValue:s,checked:l,mergedDisabled:c,cssVars:n?void 0:w,themeClass:y==null?void 0:y.themeClass,onRender:y==null?void 0:y.onRender}},render(){const{mergedClsPrefix:e,mergedDisabled:i,checked:n,mergedRailStyle:r,onRender:o,$slots:a}=this;o==null||o();const{checked:c,unchecked:u,icon:d,"checked-icon":s,"unchecked-icon":l}=a,f=!(ge(d)&&ge(s)&&ge(l));return t("div",{role:"switch","aria-checked":n,class:[`${e}-switch`,this.themeClass,f&&`${e}-switch--icon`,n&&`${e}-switch--active`,i&&`${e}-switch--disabled`,this.round&&`${e}-switch--round`,this.loading&&`${e}-switch--loading`,this.pressed&&`${e}-switch--pressed`,this.rubberBand&&`${e}-switch--rubber-band`],tabindex:this.mergedDisabled?void 0:0,style:this.cssVars,onClick:this.handleClick,onFocus:this.handleFocus,onBlur:this.handleBlur,onKeyup:this.handleKeyup,onKeydown:this.handleKeydown},t("div",{class:`${e}-switch__rail`,"aria-hidden":"true",style:r},K(c,m=>K(u,g=>m||g?t("div",{"aria-hidden":!0,class:`${e}-switch__children-placeholder`},t("div",{class:`${e}-switch__rail-placeholder`},t("div",{class:`${e}-switch__button-placeholder`}),m),t("div",{class:`${e}-switch__rail-placeholder`},t("div",{class:`${e}-switch__button-placeholder`}),g)):null)),t("div",{class:`${e}-switch__button`},K(d,m=>K(s,g=>K(l,C=>t(Fe,null,{default:()=>this.loading?t(Ve,{key:"loading",clsPrefix:e,strokeWidth:20}):this.checked&&(g||m)?t("div",{class:`${e}-switch__button-icon`,key:g?"checked-icon":"icon"},g||m):!this.checked&&(C||m)?t("div",{class:`${e}-switch__button-icon`,key:C?"unchecked-icon":"icon"},C||m):null})))),K(c,m=>m&&t("div",{key:"checked",class:`${e}-switch__checked`},m)),K(u,m=>m&&t("div",{key:"unchecked",class:`${e}-switch__unchecked`},m)))))}}),J=He("n-upload"),_e="__UPLOAD_DRAGGER__",xt=V({name:"UploadDragger",[_e]:!0,setup(e,{slots:i}){const n=ne(J,null);return n||le("upload-dragger","`n-upload-dragger` must be placed inside `n-upload`."),()=>{const{mergedClsPrefixRef:{value:r},mergedDisabledRef:{value:o},maxReachedRef:{value:a}}=n;return t("div",{class:[`${r}-upload-dragger`,(o||a)&&`${r}-upload-dragger--disabled`]},i)}}});var me=globalThis&&globalThis.__awaiter||function(e,i,n,r){function o(a){return a instanceof n?a:new n(function(c){c(a)})}return new(n||(n=Promise))(function(a,c){function u(l){try{s(r.next(l))}catch(f){c(f)}}function d(l){try{s(r.throw(l))}catch(f){c(f)}}function s(l){l.done?a(l.value):o(l.value).then(u,d)}s((r=r.apply(e,i||[])).next())})};const Oe=e=>e.includes("image/"),ke=(e="")=>{const i=e.split("/"),r=i[i.length-1].split(/#|\?/)[0];return(/\.[^./\\]*$/.exec(r)||[""])[0]},Be=/(webp|svg|png|gif|jpg|jpeg|jfif|bmp|dpg|ico)$/i,Ue=e=>{if(e.type)return Oe(e.type);const i=ke(e.name||"");if(Be.test(i))return!0;const n=e.thumbnailUrl||e.url||"",r=ke(n);return!!(/^data:image\//.test(n)||Be.test(r))};function Rt(e){return me(this,void 0,void 0,function*(){return yield new Promise(i=>{if(!e.type||!Oe(e.type)){i("");return}i(window.URL.createObjectURL(e))})})}const Ct=We&&window.FileReader&&window.File;function kt(e){return e.isDirectory}function Bt(e){return e.isFile}function $t(e,i){return me(this,void 0,void 0,function*(){const n=[];function r(o){return me(this,void 0,void 0,function*(){for(const a of o)if(a){if(i&&kt(a)){const c=a.createReader();try{const u=yield new Promise((d,s)=>{c.readEntries(d,s)});yield r(u)}catch{}}else if(Bt(a))try{const c=yield new Promise((u,d)=>{a.file(u,d)});n.push({file:c,entry:a,source:"dnd"})}catch{}}})}return yield r(e),n})}function re(e){const{id:i,name:n,percentage:r,status:o,url:a,file:c,thumbnailUrl:u,type:d,fullPath:s,batchId:l}=e;return{id:i,name:n,percentage:r??null,status:o,url:a??null,file:c??null,thumbnailUrl:u??null,type:d??null,fullPath:s??null,batchId:l??null}}function St(e,i,n){return e=e.toLowerCase(),i=i.toLocaleLowerCase(),n=n.toLocaleLowerCase(),n.split(",").map(o=>o.trim()).filter(Boolean).some(o=>{if(o.startsWith(".")){if(e.endsWith(o))return!0}else if(o.includes("/")){const[a,c]=i.split("/"),[u,d]=o.split("/");if((u==="*"||a&&u&&u===a)&&(d==="*"||c&&d&&d===c))return!0}else return!0;return!1})}const je=V({name:"UploadTrigger",props:{abstract:Boolean},setup(e,{slots:i}){const n=ne(J,null);n||le("upload-trigger","`n-upload-trigger` must be placed inside `n-upload`.");const{mergedClsPrefixRef:r,mergedDisabledRef:o,maxReachedRef:a,listTypeRef:c,dragOverRef:u,openOpenFileDialog:d,draggerInsideRef:s,handleFileAddition:l,mergedDirectoryDndRef:f,triggerClassRef:m,triggerStyleRef:g}=n,C=B(()=>c.value==="image-card");function x(){o.value||a.value||d()}function _($){$.preventDefault(),u.value=!0}function z($){$.preventDefault(),u.value=!0}function P($){$.preventDefault(),u.value=!1}function j($){var p;if($.preventDefault(),!s.value||o.value||a.value){u.value=!1;return}const w=(p=$.dataTransfer)===null||p===void 0?void 0:p.items;w!=null&&w.length?$t(Array.from(w).map(y=>y.webkitGetAsEntry()),f.value).then(y=>{l(y)}).finally(()=>{u.value=!1}):u.value=!1}return()=>{var $;const{value:p}=r;return e.abstract?($=i.default)===null||$===void 0?void 0:$.call(i,{handleClick:x,handleDrop:j,handleDragOver:_,handleDragEnter:z,handleDragLeave:P}):t("div",{class:[`${p}-upload-trigger`,(o.value||a.value)&&`${p}-upload-trigger--disabled`,C.value&&`${p}-upload-trigger--image-card`,m.value],style:g.value,onClick:x,onDrop:j,onDragover:_,onDragenter:z,onDragleave:P},C.value?t(xt,null,{default:()=>qe(i.default,()=>[t(M,{clsPrefix:p},{default:()=>t(Xe,null)})])}):i)}}}),Pt=V({name:"UploadProgress",props:{show:Boolean,percentage:{type:Number,required:!0},status:{type:String,required:!0}},setup(){return{mergedTheme:ne(J).mergedThemeRef}},render(){return t(Ie,null,{default:()=>this.show?t(bt,{type:"line",showIndicator:!1,percentage:this.percentage,status:this.status,height:2,theme:this.mergedTheme.peers.Progress,themeOverrides:this.mergedTheme.peerOverrides.Progress}):null})}}),Tt=t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 28 28"},t("g",{fill:"none"},t("path",{d:"M21.75 3A3.25 3.25 0 0 1 25 6.25v15.5A3.25 3.25 0 0 1 21.75 25H6.25A3.25 3.25 0 0 1 3 21.75V6.25A3.25 3.25 0 0 1 6.25 3h15.5zm.583 20.4l-7.807-7.68a.75.75 0 0 0-.968-.07l-.084.07l-7.808 7.68c.183.065.38.1.584.1h15.5c.204 0 .4-.035.583-.1l-7.807-7.68l7.807 7.68zM21.75 4.5H6.25A1.75 1.75 0 0 0 4.5 6.25v15.5c0 .208.036.408.103.593l7.82-7.692a2.25 2.25 0 0 1 3.026-.117l.129.117l7.82 7.692c.066-.185.102-.385.102-.593V6.25a1.75 1.75 0 0 0-1.75-1.75zm-3.25 3a2.5 2.5 0 1 1 0 5a2.5 2.5 0 0 1 0-5zm0 1.5a1 1 0 1 0 0 2a1 1 0 0 0 0-2z",fill:"currentColor"}))),zt=t("svg",{xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 28 28"},t("g",{fill:"none"},t("path",{d:"M6.4 2A2.4 2.4 0 0 0 4 4.4v19.2A2.4 2.4 0 0 0 6.4 26h15.2a2.4 2.4 0 0 0 2.4-2.4V11.578c0-.729-.29-1.428-.805-1.944l-6.931-6.931A2.4 2.4 0 0 0 14.567 2H6.4zm-.9 2.4a.9.9 0 0 1 .9-.9H14V10a2 2 0 0 0 2 2h6.5v11.6a.9.9 0 0 1-.9.9H6.4a.9.9 0 0 1-.9-.9V4.4zm16.44 6.1H16a.5.5 0 0 1-.5-.5V4.06l6.44 6.44z",fill:"currentColor"})));var Lt=globalThis&&globalThis.__awaiter||function(e,i,n,r){function o(a){return a instanceof n?a:new n(function(c){c(a)})}return new(n||(n=Promise))(function(a,c){function u(l){try{s(r.next(l))}catch(f){c(f)}}function d(l){try{s(r.throw(l))}catch(f){c(f)}}function s(l){l.done?a(l.value):o(l.value).then(u,d)}s((r=r.apply(e,i||[])).next())})};const ae={paddingMedium:"0 3px",heightMedium:"24px",iconSizeMedium:"18px"},Dt=V({name:"UploadFile",props:{clsPrefix:{type:String,required:!0},file:{type:Object,required:!0},listType:{type:String,required:!0}},setup(e){const i=ne(J),n=X(null),r=X(""),o=B(()=>{const{file:p}=e;return p.status==="finished"?"success":p.status==="error"?"error":"info"}),a=B(()=>{const{file:p}=e;if(p.status==="error")return"error"}),c=B(()=>{const{file:p}=e;return p.status==="uploading"}),u=B(()=>{if(!i.showCancelButtonRef.value)return!1;const{file:p}=e;return["uploading","pending","error"].includes(p.status)}),d=B(()=>{if(!i.showRemoveButtonRef.value)return!1;const{file:p}=e;return["finished"].includes(p.status)}),s=B(()=>{if(!i.showDownloadButtonRef.value)return!1;const{file:p}=e;return["finished"].includes(p.status)}),l=B(()=>{if(!i.showRetryButtonRef.value)return!1;const{file:p}=e;return["error"].includes(p.status)}),f=Ge(()=>r.value||e.file.thumbnailUrl||e.file.url),m=B(()=>{if(!i.showPreviewButtonRef.value)return!1;const{file:{status:p},listType:w}=e;return["finished"].includes(p)&&f.value&&w==="image-card"});function g(){i.submit(e.file.id)}function C(p){p.preventDefault();const{file:w}=e;["finished","pending","error"].includes(w.status)?_(w):["uploading"].includes(w.status)?P(w):Qe("upload","The button clicked type is unknown.")}function x(p){p.preventDefault(),z(e.file)}function _(p){const{xhrMap:w,doChange:y,onRemoveRef:{value:k},mergedFileListRef:{value:h}}=i;Promise.resolve(k?k({file:Object.assign({},p),fileList:h}):!0).then(S=>{if(S===!1)return;const T=Object.assign({},p,{status:"removed"});w.delete(p.id),y(T,void 0,{remove:!0})})}function z(p){const{onDownloadRef:{value:w}}=i;Promise.resolve(w?w(Object.assign({},p)):!0).then(y=>{y!==!1&&Je(p.url,p.name)})}function P(p){const{xhrMap:w}=i,y=w.get(p.id);y==null||y.abort(),_(Object.assign({},p))}function j(){const{onPreviewRef:{value:p}}=i;if(p)p(e.file);else if(e.listType==="image-card"){const{value:w}=n;if(!w)return;w.click()}}const $=()=>Lt(this,void 0,void 0,function*(){const{listType:p}=e;p!=="image"&&p!=="image-card"||i.shouldUseThumbnailUrlRef.value(e.file)&&(r.value=yield i.getFileThumbnailUrlResolver(e.file))});return Ke(()=>{$()}),{mergedTheme:i.mergedThemeRef,progressStatus:o,buttonType:a,showProgress:c,disabled:i.mergedDisabledRef,showCancelButton:u,showRemoveButton:d,showDownloadButton:s,showRetryButton:l,showPreviewButton:m,mergedThumbnailUrl:f,shouldUseThumbnailUrl:i.shouldUseThumbnailUrlRef,renderIcon:i.renderIconRef,imageRef:n,handleRemoveOrCancelClick:C,handleDownloadClick:x,handleRetryClick:g,handlePreviewClick:j}},render(){const{clsPrefix:e,mergedTheme:i,listType:n,file:r,renderIcon:o}=this;let a;const c=n==="image";c||n==="image-card"?a=!this.shouldUseThumbnailUrl(r)||!this.mergedThumbnailUrl?t("span",{class:`${e}-upload-file-info__thumbnail`},o?o(r):Ue(r)?t(M,{clsPrefix:e},{default:()=>Tt}):t(M,{clsPrefix:e},{default:()=>zt})):t("a",{rel:"noopener noreferer",target:"_blank",href:r.url||void 0,class:`${e}-upload-file-info__thumbnail`,onClick:this.handlePreviewClick},n==="image-card"?t(Ye,{src:this.mergedThumbnailUrl||void 0,previewSrc:r.url||void 0,alt:r.name,ref:"imageRef"}):t("img",{src:this.mergedThumbnailUrl||void 0,alt:r.name})):a=t("span",{class:`${e}-upload-file-info__thumbnail`},o?o(r):t(M,{clsPrefix:e},{default:()=>t(at,null)}));const d=t(Pt,{show:this.showProgress,percentage:r.percentage||0,status:this.progressStatus}),s=n==="text"||n==="image";return t("div",{class:[`${e}-upload-file`,`${e}-upload-file--${this.progressStatus}-status`,r.url&&r.status!=="error"&&n!=="image-card"&&`${e}-upload-file--with-url`,`${e}-upload-file--${n}-type`]},t("div",{class:`${e}-upload-file-info`},a,t("div",{class:`${e}-upload-file-info__name`},s&&(r.url&&r.status!=="error"?t("a",{rel:"noopener noreferer",target:"_blank",href:r.url||void 0,onClick:this.handlePreviewClick},r.name):t("span",{onClick:this.handlePreviewClick},r.name)),c&&d),t("div",{class:[`${e}-upload-file-info__action`,`${e}-upload-file-info__action--${n}-type`]},this.showPreviewButton?t(oe,{key:"preview",quaternary:!0,type:this.buttonType,onClick:this.handlePreviewClick,theme:i.peers.Button,themeOverrides:i.peerOverrides.Button,builtinThemeOverrides:ae},{icon:()=>t(M,{clsPrefix:e},{default:()=>t(Ze,null)})}):null,(this.showRemoveButton||this.showCancelButton)&&!this.disabled&&t(oe,{key:"cancelOrTrash",theme:i.peers.Button,themeOverrides:i.peerOverrides.Button,quaternary:!0,builtinThemeOverrides:ae,type:this.buttonType,onClick:this.handleRemoveOrCancelClick},{icon:()=>t(Fe,null,{default:()=>this.showRemoveButton?t(M,{clsPrefix:e,key:"trash"},{default:()=>t(lt,null)}):t(M,{clsPrefix:e,key:"cancel"},{default:()=>t(dt,null)})})}),this.showRetryButton&&!this.disabled&&t(oe,{key:"retry",quaternary:!0,type:this.buttonType,onClick:this.handleRetryClick,theme:i.peers.Button,themeOverrides:i.peerOverrides.Button,builtinThemeOverrides:ae},{icon:()=>t(M,{clsPrefix:e},{default:()=>t(ct,null)})}),this.showDownloadButton?t(oe,{key:"download",quaternary:!0,type:this.buttonType,onClick:this.handleDownloadClick,theme:i.peers.Button,themeOverrides:i.peerOverrides.Button,builtinThemeOverrides:ae},{icon:()=>t(M,{clsPrefix:e},{default:()=>t(st,null)})}):null)),!c&&d)}}),Ft=V({name:"UploadFileList",setup(e,{slots:i}){const n=ne(J,null);n||le("upload-file-list","`n-upload-file-list` must be placed inside `n-upload`.");const{abstractRef:r,mergedClsPrefixRef:o,listTypeRef:a,mergedFileListRef:c,fileListClassRef:u,fileListStyleRef:d,cssVarsRef:s,themeClassRef:l,maxReachedRef:f,showTriggerRef:m,imageGroupPropsRef:g}=n,C=B(()=>a.value==="image-card"),x=()=>c.value.map(z=>t(Dt,{clsPrefix:o.value,key:z.id,file:z,listType:a.value})),_=()=>C.value?t(et,Object.assign({},g.value),{default:x}):t(Ie,{group:!0},{default:x});return()=>{const{value:z}=o,{value:P}=r;return t("div",{class:[`${z}-upload-file-list`,C.value&&`${z}-upload-file-list--grid`,P?l==null?void 0:l.value:void 0,u.value],style:[P&&s?s.value:"",d.value]},_(),m.value&&!f.value&&C.value&&t(je,null,i))}}}),It=L([v("upload","width: 100%;",[R("dragger-inside",[v("upload-trigger",`
 display: block;
 `)]),R("drag-over",[v("upload-dragger",`
 border: var(--n-dragger-border-hover);
 `)])]),v("upload-dragger",`
 cursor: pointer;
 box-sizing: border-box;
 width: 100%;
 text-align: center;
 border-radius: var(--n-border-radius);
 padding: 24px;
 opacity: 1;
 transition:
 opacity .3s var(--n-bezier),
 border-color .3s var(--n-bezier),
 background-color .3s var(--n-bezier);
 background-color: var(--n-dragger-color);
 border: var(--n-dragger-border);
 `,[L("&:hover",`
 border: var(--n-dragger-border-hover);
 `),R("disabled",`
 cursor: not-allowed;
 `)]),v("upload-trigger",`
 display: inline-block;
 box-sizing: border-box;
 opacity: 1;
 transition: opacity .3s var(--n-bezier);
 `,[L("+",[v("upload-file-list","margin-top: 8px;")]),R("disabled",`
 opacity: var(--n-item-disabled-opacity);
 cursor: not-allowed;
 `),R("image-card",`
 width: 96px;
 height: 96px;
 `,[v("base-icon",`
 font-size: 24px;
 `),v("upload-dragger",`
 padding: 0;
 height: 100%;
 width: 100%;
 display: flex;
 align-items: center;
 justify-content: center;
 `)])]),v("upload-file-list",`
 line-height: var(--n-line-height);
 opacity: 1;
 transition: opacity .3s var(--n-bezier);
 `,[L("a, img","outline: none;"),R("disabled",`
 opacity: var(--n-item-disabled-opacity);
 cursor: not-allowed;
 `,[v("upload-file","cursor: not-allowed;")]),R("grid",`
 display: grid;
 grid-template-columns: repeat(auto-fill, 96px);
 grid-gap: 8px;
 margin-top: 0;
 `),v("upload-file",`
 display: block;
 box-sizing: border-box;
 cursor: default;
 padding: 0px 12px 0 6px;
 transition: background-color .3s var(--n-bezier);
 border-radius: var(--n-border-radius);
 `,[xe(),v("progress",[xe({foldPadding:!0})]),L("&:hover",`
 background-color: var(--n-item-color-hover);
 `,[v("upload-file-info",[b("action",`
 opacity: 1;
 `)])]),R("image-type",`
 border-radius: var(--n-border-radius);
 text-decoration: underline;
 text-decoration-color: #0000;
 `,[v("upload-file-info",`
 padding-top: 0px;
 padding-bottom: 0px;
 width: 100%;
 height: 100%;
 display: flex;
 justify-content: space-between;
 align-items: center;
 padding: 6px 0;
 `,[v("progress",`
 padding: 2px 0;
 margin-bottom: 0;
 `),b("name",`
 padding: 0 8px;
 `),b("thumbnail",`
 width: 32px;
 height: 32px;
 font-size: 28px;
 display: flex;
 justify-content: center;
 align-items: center;
 `,[L("img",`
 width: 100%;
 `)])])]),R("text-type",[v("progress",`
 box-sizing: border-box;
 padding-bottom: 6px;
 margin-bottom: 6px;
 `)]),R("image-card-type",`
 position: relative;
 width: 96px;
 height: 96px;
 border: var(--n-item-border-image-card);
 border-radius: var(--n-border-radius);
 padding: 0;
 display: flex;
 align-items: center;
 justify-content: center;
 transition: border-color .3s var(--n-bezier), background-color .3s var(--n-bezier);
 border-radius: var(--n-border-radius);
 overflow: hidden;
 `,[v("progress",`
 position: absolute;
 left: 8px;
 bottom: 8px;
 right: 8px;
 width: unset;
 `),v("upload-file-info",`
 padding: 0;
 width: 100%;
 height: 100%;
 `,[b("thumbnail",`
 width: 100%;
 height: 100%;
 display: flex;
 flex-direction: column;
 align-items: center;
 justify-content: center;
 font-size: 36px;
 `,[L("img",`
 width: 100%;
 `)])]),L("&::before",`
 position: absolute;
 z-index: 1;
 left: 0;
 right: 0;
 top: 0;
 bottom: 0;
 border-radius: inherit;
 opacity: 0;
 transition: opacity .2s var(--n-bezier);
 content: "";
 `),L("&:hover",[L("&::before","opacity: 1;"),v("upload-file-info",[b("thumbnail","opacity: .12;")])])]),R("error-status",[L("&:hover",`
 background-color: var(--n-item-color-hover-error);
 `),v("upload-file-info",[b("name","color: var(--n-item-text-color-error);"),b("thumbnail","color: var(--n-item-text-color-error);")]),R("image-card-type",`
 border: var(--n-item-border-image-card-error);
 `)]),R("with-url",`
 cursor: pointer;
 `,[v("upload-file-info",[b("name",`
 color: var(--n-item-text-color-success);
 text-decoration-color: var(--n-item-text-color-success);
 `,[L("a",`
 text-decoration: underline;
 `)])])]),v("upload-file-info",`
 position: relative;
 padding-top: 6px;
 padding-bottom: 6px;
 display: flex;
 flex-wrap: nowrap;
 `,[b("thumbnail",`
 font-size: 18px;
 opacity: 1;
 transition: opacity .2s var(--n-bezier);
 color: var(--n-item-icon-color);
 `,[v("base-icon",`
 margin-right: 2px;
 vertical-align: middle;
 transition: color .3s var(--n-bezier);
 `)]),b("action",`
 padding-top: inherit;
 padding-bottom: inherit;
 position: absolute;
 right: 0;
 top: 0;
 bottom: 0;
 width: 80px;
 display: flex;
 align-items: center;
 transition: opacity .2s var(--n-bezier);
 justify-content: flex-end;
 opacity: 0;
 `,[v("button",[L("&:not(:last-child)",{marginRight:"4px"}),v("base-icon",[L("svg",[ve()])])]),R("image-type",`
 position: relative;
 max-width: 80px;
 width: auto;
 `),R("image-card-type",`
 z-index: 2;
 position: absolute;
 width: 100%;
 height: 100%;
 left: 0;
 right: 0;
 bottom: 0;
 top: 0;
 display: flex;
 justify-content: center;
 align-items: center;
 `)]),b("name",`
 color: var(--n-item-text-color);
 flex: 1;
 display: flex;
 justify-content: center;
 text-overflow: ellipsis;
 overflow: hidden;
 flex-direction: column;
 text-decoration-color: #0000;
 font-size: var(--n-font-size);
 transition:
 color .3s var(--n-bezier),
 text-decoration-color .3s var(--n-bezier); 
 `,[L("a",`
 color: inherit;
 text-decoration: underline;
 `)])])])]),v("upload-file-input",`
 display: none;
 width: 0;
 height: 0;
 opacity: 0;
 `)]);var $e=globalThis&&globalThis.__awaiter||function(e,i,n,r){function o(a){return a instanceof n?a:new n(function(c){c(a)})}return new(n||(n=Promise))(function(a,c){function u(l){try{s(r.next(l))}catch(f){c(f)}}function d(l){try{s(r.throw(l))}catch(f){c(f)}}function s(l){l.done?a(l.value):o(l.value).then(u,d)}s((r=r.apply(e,i||[])).next())})};function _t(e,i,n){const{doChange:r,xhrMap:o}=e;let a=0;function c(d){var s;let l=Object.assign({},i,{status:"error",percentage:a});o.delete(i.id),l=re(((s=e.onError)===null||s===void 0?void 0:s.call(e,{file:l,event:d}))||l),r(l,d)}function u(d){var s;if(e.isErrorState){if(e.isErrorState(n)){c(d);return}}else if(n.status<200||n.status>=300){c(d);return}let l=Object.assign({},i,{status:"finished",percentage:a});o.delete(i.id),l=re(((s=e.onFinish)===null||s===void 0?void 0:s.call(e,{file:l,event:d}))||l),r(l,d)}return{handleXHRLoad:u,handleXHRError:c,handleXHRAbort(d){const s=Object.assign({},i,{status:"removed",file:null,percentage:a});o.delete(i.id),r(s,d)},handleXHRProgress(d){const s=Object.assign({},i,{status:"uploading"});if(d.lengthComputable){const l=Math.ceil(d.loaded/d.total*100);s.percentage=l,a=l}r(s,d)}}}function Ot(e){const{inst:i,file:n,data:r,headers:o,withCredentials:a,action:c,customRequest:u}=e,{doChange:d}=e.inst;let s=0;u({file:n,data:r,headers:o,withCredentials:a,action:c,onProgress(l){const f=Object.assign({},n,{status:"uploading"}),m=l.percent;f.percentage=m,s=m,d(f)},onFinish(){var l;let f=Object.assign({},n,{status:"finished",percentage:s});f=re(((l=i.onFinish)===null||l===void 0?void 0:l.call(i,{file:f}))||f),d(f)},onError(){var l;let f=Object.assign({},n,{status:"error",percentage:s});f=re(((l=i.onError)===null||l===void 0?void 0:l.call(i,{file:f}))||f),d(f)}})}function Ut(e,i,n){const r=_t(e,i,n);n.onabort=r.handleXHRAbort,n.onerror=r.handleXHRError,n.onload=r.handleXHRLoad,n.upload&&(n.upload.onprogress=r.handleXHRProgress)}function Ne(e,i){return typeof e=="function"?e({file:i}):e||{}}function jt(e,i,n){const r=Ne(i,n);r&&Object.keys(r).forEach(o=>{e.setRequestHeader(o,r[o])})}function Nt(e,i,n){const r=Ne(i,n);r&&Object.keys(r).forEach(o=>{e.append(o,r[o])})}function Et(e,i,n,{method:r,action:o,withCredentials:a,responseType:c,headers:u,data:d}){const s=new XMLHttpRequest;s.responseType=c,e.xhrMap.set(n.id,s),s.withCredentials=a;const l=new FormData;if(Nt(l,d,n),n.file!==null&&l.append(i,n.file),Ut(e,n,s),o!==void 0){s.open(r.toUpperCase(),o),jt(s,u,n),s.send(l);const f=Object.assign({},n,{status:"uploading"});e.doChange(f)}}const At=Object.assign(Object.assign({},Q.props),{name:{type:String,default:"file"},accept:String,action:String,customRequest:Function,directory:Boolean,directoryDnd:{type:Boolean,default:void 0},method:{type:String,default:"POST"},multiple:Boolean,showFileList:{type:Boolean,default:!0},data:[Object,Function],headers:[Object,Function],withCredentials:Boolean,responseType:{type:String,default:""},disabled:{type:Boolean,default:void 0},onChange:Function,onRemove:Function,onFinish:Function,onError:Function,onBeforeUpload:Function,isErrorState:Function,onDownload:Function,defaultUpload:{type:Boolean,default:!0},fileList:Array,"onUpdate:fileList":[Function,Array],onUpdateFileList:[Function,Array],fileListClass:String,fileListStyle:[String,Object],defaultFileList:{type:Array,default:()=>[]},showCancelButton:{type:Boolean,default:!0},showRemoveButton:{type:Boolean,default:!0},showDownloadButton:Boolean,showRetryButton:{type:Boolean,default:!0},showPreviewButton:{type:Boolean,default:!0},listType:{type:String,default:"text"},onPreview:Function,shouldUseThumbnailUrl:{type:Function,default:e=>Ct?Ue(e):!1},createThumbnailUrl:Function,abstract:Boolean,max:Number,showTrigger:{type:Boolean,default:!0},imageGroupProps:Object,inputProps:Object,triggerClass:String,triggerStyle:[String,Object],renderIcon:Function}),Ht=V({name:"Upload",props:At,setup(e){e.abstract&&e.listType==="image-card"&&le("upload","when the list-type is image-card, abstract is not supported.");const{mergedClsPrefixRef:i,inlineThemeDisabled:n}=be(e),r=Q("Upload","-upload",It,nt,e,i),o=Le(e),a=B(()=>{const{max:h}=e;return h!==void 0?g.value.length>=h:!1}),c=X(e.defaultFileList),u=D(e,"fileList"),d=X(null),s={value:!1},l=X(!1),f=new Map,m=De(u,c),g=B(()=>m.value.map(re));function C(){var h;(h=d.value)===null||h===void 0||h.click()}function x(h){const S=h.target;P(S.files?Array.from(S.files).map(T=>({file:T,entry:null,source:"input"})):null,h),S.value=""}function _(h){const{"onUpdate:fileList":S,onUpdateFileList:T}=e;S&&te(S,h),T&&te(T,h),c.value=h}const z=B(()=>e.multiple||e.directory);function P(h,S){if(!h||h.length===0)return;const{onBeforeUpload:T}=e;h=z.value?h:[h[0]];const{max:E,accept:U}=e;h=h.filter(({file:F,source:I})=>I==="dnd"&&(U!=null&&U.trim())?St(F.name,F.type,U):!0),E&&(h=h.slice(0,E-g.value.length));const N=Re();Promise.all(h.map(({file:F,entry:I})=>$e(this,void 0,void 0,function*(){var A;const O={id:Re(),batchId:N,name:F.name,status:"pending",percentage:0,file:F,url:null,type:F.type,thumbnailUrl:null,fullPath:(A=I==null?void 0:I.fullPath)!==null&&A!==void 0?A:`/${F.webkitRelativePath||F.name}`};return!T||(yield T({file:O,fileList:g.value}))!==!1?O:null}))).then(F=>$e(this,void 0,void 0,function*(){let I=Promise.resolve();F.forEach(A=>{I=I.then(ot).then(()=>{A&&$(A,S,{append:!0})})}),yield I})).then(()=>{e.defaultUpload&&j()})}function j(h){const{method:S,action:T,withCredentials:E,headers:U,data:N,name:F}=e,I=h!==void 0?g.value.filter(O=>O.id===h):g.value,A=h!==void 0;I.forEach(O=>{const{status:Y}=O;(Y==="pending"||Y==="error"&&A)&&(e.customRequest?Ot({inst:{doChange:$,xhrMap:f,onFinish:e.onFinish,onError:e.onError},file:O,action:T,withCredentials:E,headers:U,data:N,customRequest:e.customRequest}):Et({doChange:$,xhrMap:f,onFinish:e.onFinish,onError:e.onError,isErrorState:e.isErrorState},F,O,{method:S,action:T,withCredentials:E,responseType:e.responseType,headers:U,data:N}))})}const $=(h,S,T={append:!1,remove:!1})=>{const{append:E,remove:U}=T,N=Array.from(g.value),F=N.findIndex(I=>I.id===h.id);if(E||U||~F){E?N.push(h):U?N.splice(F,1):N.splice(F,1,h);const{onChange:I}=e;I&&I({file:h,fileList:N,event:S}),_(N)}};function p(h){var S;if(h.thumbnailUrl)return h.thumbnailUrl;const{createThumbnailUrl:T}=e;return T?(S=T(h.file,h))!==null&&S!==void 0?S:h.url||"":h.url?h.url:h.file?Rt(h.file):""}const w=B(()=>{const{common:{cubicBezierEaseInOut:h},self:{draggerColor:S,draggerBorder:T,draggerBorderHover:E,itemColorHover:U,itemColorHoverError:N,itemTextColorError:F,itemTextColorSuccess:I,itemTextColor:A,itemIconColor:O,itemDisabledOpacity:Y,lineHeight:se,borderRadius:H,fontSize:G,itemBorderImageCardError:de,itemBorderImageCard:ce}}=r.value;return{"--n-bezier":h,"--n-border-radius":H,"--n-dragger-border":T,"--n-dragger-border-hover":E,"--n-dragger-color":S,"--n-font-size":G,"--n-item-color-hover":U,"--n-item-color-hover-error":N,"--n-item-disabled-opacity":Y,"--n-item-icon-color":O,"--n-item-text-color":A,"--n-item-text-color-error":F,"--n-item-text-color-success":I,"--n-line-height":se,"--n-item-border-image-card-error":de,"--n-item-border-image-card":ce}}),y=n?ye("upload",void 0,w,e):void 0;tt(J,{mergedClsPrefixRef:i,mergedThemeRef:r,showCancelButtonRef:D(e,"showCancelButton"),showDownloadButtonRef:D(e,"showDownloadButton"),showRemoveButtonRef:D(e,"showRemoveButton"),showRetryButtonRef:D(e,"showRetryButton"),onRemoveRef:D(e,"onRemove"),onDownloadRef:D(e,"onDownload"),mergedFileListRef:g,triggerClassRef:D(e,"triggerClass"),triggerStyleRef:D(e,"triggerStyle"),shouldUseThumbnailUrlRef:D(e,"shouldUseThumbnailUrl"),renderIconRef:D(e,"renderIcon"),xhrMap:f,submit:j,doChange:$,showPreviewButtonRef:D(e,"showPreviewButton"),onPreviewRef:D(e,"onPreview"),getFileThumbnailUrlResolver:p,listTypeRef:D(e,"listType"),dragOverRef:l,openOpenFileDialog:C,draggerInsideRef:s,handleFileAddition:P,mergedDisabledRef:o.mergedDisabledRef,maxReachedRef:a,fileListClassRef:D(e,"fileListClass"),fileListStyleRef:D(e,"fileListStyle"),abstractRef:D(e,"abstract"),acceptRef:D(e,"accept"),cssVarsRef:n?void 0:w,themeClassRef:y==null?void 0:y.themeClass,onRender:y==null?void 0:y.onRender,showTriggerRef:D(e,"showTrigger"),imageGroupPropsRef:D(e,"imageGroupProps"),mergedDirectoryDndRef:B(()=>{var h;return(h=e.directoryDnd)!==null&&h!==void 0?h:e.directory})});const k={clear:()=>{c.value=[]},submit:j,openOpenFileDialog:C};return Object.assign({mergedClsPrefix:i,draggerInsideRef:s,inputElRef:d,mergedTheme:r,dragOver:l,mergedMultiple:z,cssVars:n?void 0:w,themeClass:y==null?void 0:y.themeClass,onRender:y==null?void 0:y.onRender,handleFileInputChange:x},k)},render(){var e,i;const{draggerInsideRef:n,mergedClsPrefix:r,$slots:o,directory:a,onRender:c}=this;if(o.default&&!this.abstract){const d=o.default()[0];!((e=d==null?void 0:d.type)===null||e===void 0)&&e[_e]&&(n.value=!0)}const u=t("input",Object.assign({},this.inputProps,{ref:"inputElRef",type:"file",class:`${r}-upload-file-input`,accept:this.accept,multiple:this.mergedMultiple,onChange:this.handleFileInputChange,webkitdirectory:a||void 0,directory:a||void 0}));return this.abstract?t(it,null,(i=o.default)===null||i===void 0?void 0:i.call(o),t(rt,{to:"body"},u)):(c==null||c(),t("div",{class:[`${r}-upload`,n.value&&`${r}-upload--dragger-inside`,this.dragOver&&`${r}-upload--drag-over`,this.themeClass],style:this.cssVars},u,this.showTrigger&&this.listType!=="image-card"&&t(je,null,o),this.showFileList&&t(Ft,null,o)))}});export{Vt as N,Ht as a,xt as b};

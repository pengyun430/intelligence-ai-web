module.exports = {
  privateKey: '/Users/edz/.ssh/id_rsa', // 本地私钥地址，位置一般在/Users/xxx/.ssh/id_rsa，非必填，有私钥则配置
  passphrase: '', // 本地私钥密码，非必填，有私钥则配置
  projectName: '智能AI', // 项目名称
  prod: {
    // 标准版测试环境
    name: '智能AI标准版',
    script: 'npm run build-only', // 测试环境打包脚本
    host: '49.232.169.110', // 测试服务器地址
    port: 22, // ssh port，一般默认22
    username: 'ubuntu', // 登录服务器用户名
    password: 'H-!+m3aCc', // 登录服务器密码
    distPath: 'dist', // 本地打包web-admin目录
    webDir: '/data/app_front/aideepin' // 测试环境服务器地址
  }
}
